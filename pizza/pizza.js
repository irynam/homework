

const CHEESE_IMG = [
     "./img/Cheese/Chedar.jpg"
    ,"./img/Cheese/dor_blu.png"
    ,"./img/Cheese/feta.jpg"
    ,"./img/Cheese/mozzarella.png"
    ,"./img/Cheese/parmesan.png"
]
const MEAT_IMG = [
    "./img/Meat/bacon.jpg"
   ,"./img/Meat/bavarian.jpg"
   ,"./img/Meat/chicken.jpg"
   ,"./img/Meat/ham.jpg"
   ,"./img/Meat/meatballs.png"
   ,"./img/Meat/peperoni.jpg"
   ,"./img/Meat/tuna.jpg"
]
const SAUCES_IMG = [
    "./img/Sauces/alfredo.jpg"
    ,"./img/Sauces/bbq.png"
    ,"./img/Sauces/garlic.png"
]
const VEGETABLES_IMG = [
    "./img/Vegetables/corn.jpeg"
    ,"./img/Vegetables/green_pepper.png"
    ,"./img/Vegetables/jalapeno.jpg"
    ,"./img/Vegetables/mushrooms.png"
    ,"./img/Vegetables/olives.jpg"
    ,"./img/Vegetables/onion.jpg"
    ,"./img/Vegetables/pineapple.png"
    ,"./img/Vegetables/tomatoes.png"
]

class Ingredient {
    img = "";
    price = 0;
    constructor(img, price){
        this.img = img;
        this.price = price;
    }

    get image() {
        return this.img;
    }

    get price() {
        return this.price
    }
}

class Pizza {
    ingredients = new Array;
    price = 0;
    size = 1;

    addIngredient(in_ingredient){
        this.ingredients.push(in_ingredient);
    }

    removeIngredient(in_ingredient){
        this.ingredients.filter(item => item !== in_ingredient)
    }

    calculatePrice(){
        this.price = 0;
        this.ingredients.forEach(item => {
            this.price += Number(item.price);
        })
    }

    get price() {
        return this.price;
    }

}
let pizza = new Pizza();


window.onload = ()=>{ 
    document.getElementById("Slider").innerHTML = "<object type  = 'text/html' data = 'slider/index.html'>";


    let priceLabel = document.getElementById("Price");
    priceLabel.innerHTML = `Price: ${pizza.price}`;

    let ingredientBtns = document.getElementsByClassName("btnIngredient");
    for (let i = 0; i < ingredientBtns.length; i++) {
        ingredientBtns[i].addEventListener("click", onIngredientTypeClick);
    }

    function onIngredientClick(e){
        let ing = new Ingredient(this.src, this.dataset.price);
        pizza.addIngredient(ing);
        updatePrice()
    }

    function updatePrice(){
        pizza.calculatePrice();
        priceLabel.innerHTML = `Price: ${pizza.price}`;
    }

    function onIngredientTypeClick(e){
        let img = [];
        switch(this.id){
            case "Cheese":
                img = CHEESE_IMG;
                break;
            case "Meat":
                img = MEAT_IMG;
                break;
            case "Sauces":
                img = SAUCES_IMG;
                break;
            case "Vegetables":
                img = VEGETABLES_IMG;
                break;
            case "Additional":
                break;
        }
        let aside = document.getElementById("aside");
        while (aside.hasChildNodes()) {  
            aside.removeChild(aside.firstChild);
          }
        img.forEach((item) => {
            let elem = document.createElement("img");
            let price = Math.floor(Math.random() * Math.floor(15))
            elem.setAttribute("src", item);
            elem.setAttribute('data-price', price);
            elem.title = `Price: ${price}`;
            aside.appendChild(elem);
            elem.addEventListener("mousedown", onMouseDown);   
            elem.ondragstart = function() {
                return false;
              };
  
        })
    }

    function onMouseDown(event){
        let currentDroppable = null;
        let target = this//.cloneNode(true);
        let shiftX = event.clientX - this.getBoundingClientRect().left;
        let shiftY = event.clientY - this.getBoundingClientRect().top;
  
        target.style.position = 'absolute';
        target.style.zIndex = 1000;
        document.body.append(target);
  
        moveAt(event.pageX, event.pageY);
  
        function moveAt(pageX, pageY) {
            target.style.left = pageX - shiftX + 'px';
            target.style.top = pageY - shiftY + 'px';
        }

        document.addEventListener("mousemove", onMouseMove);
        document.addEventListener("mouseup", onMouseUp);

        function onMouseUp (event) {
            document.removeEventListener('mousemove', onMouseMove);
            target.onmouseup = null;

            target.hidden = true;
            let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
            target.hidden = false;
            if (elemBelow.id =="Constructor") {
                pizza.addIngredient(new Ingredient(target.src, target.dataset.price));
                updatePrice();
            }
        }; 
  
        function onMouseMove(event) {
          moveAt(event.pageX, event.pageY);
        }
    }
}