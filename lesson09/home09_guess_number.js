window.addEventListener("load", function () {
    let randomNumber = Math.floor(Math.random() * 100) + 1;

    const guesses = document.querySelector('.guesses');
    const lastResult = document.querySelector('.lastResult');
    const lowOrHi = document.querySelector('.lowOrHi');
    
    const guessSubmit = document.querySelector('.guessSubmit');
    const guessField = document.querySelector('.guessField');
    
    let guessCount = 1;
    let newGameButton;

    let checkInputNumber = () => {
        let inputNumber = Number(guessField.value);
        if (guessCount === 1) {
          guesses.textContent = 'Previous guesses: ';
        }
        guesses.textContent += inputNumber + ' ';
       
        if (inputNumber === randomNumber) {
          lastResult.textContent = 'Congratulations! You got it right!';
          lastResult.style.backgroundColor = 'green';
          lowOrHi.textContent = '';
          setGameOver();
        } else if (guessCount === 10) {
          lastResult.textContent = '!!!GAME OVER!!!';
          setGameOver();
        } else {
          lastResult.textContent = 'Wrong!';
          lastResult.style.backgroundColor = 'white';
          lastResult.style.color = "#ff4400"
          if(inputNumber < randomNumber) {
            lowOrHi.textContent = 'Last guess was too low!';
          } else if(inputNumber > randomNumber) {
            lowOrHi.textContent = 'Last guess was too high!';
          }
        }
       
        guessCount++;
        guessField.value = '';
        guessField.focus();
    }

    guessSubmit.addEventListener('click', checkInputNumber);

    let setGameOver = () => {
        guessField.disabled = true;
        guessSubmit.disabled = true;
        newGameButton = document.createElement('button');
        newGameButton.textContent = 'Start new game';
        document.body.appendChild(newGameButton);
        newGameButton.addEventListener('click', startNewGame);
    }

    let startNewGame = () => {
        guessCount = 1;
        var resetParas = document.querySelectorAll('.resultParas p');
        for(var i = 0 ; i < resetParas.length ; i++) {
            resetParas[i].textContent = '';
        }
        
        newGameButton.parentNode.removeChild(newGameButton);
        guessField.disabled = false;
        guessSubmit.disabled = false;
        guessField.value = '';
        guessField.focus();
        lastResult.style.backgroundColor = 'white';
        randomNumber = Math.floor(Math.random() * 100) + 1;
    }

} , false)