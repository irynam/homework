window.addEventListener("load", function () {

  const loginField = document.querySelector(".login_form");
  const passwordField = document.querySelector(".password_form");
  const authorization = document.querySelector('.btn');
  const results = document.querySelector('.results');
    
  let reset = () => {
    results.textContent = "";
    results.backgroundColor = "white";
    loginField.style.borderColor = "initial";
    passwordField.style.borderColor = "initial";
  }

  loginField.addEventListener('input', reset);
  passwordField.addEventListener('input', reset); 

  let checkInput = () => {
    let inputLogin = loginField.value;
    let inputPassword = passwordField.value;
    if (inputLogin == "" && inputPassword == ""){
      results.textContent = 'Login and password form is empty!';
      results.style.backgroundColor = 'red';
      loginField.style.borderColor = 'red';
      passwordField.style.borderColor = 'red';
    } else if (inputLogin == ""){
      results.textContent = 'Login form is empty!';
      results.style.backgroundColor = 'red';
      loginField.style.borderColor = 'red';
    } else if (inputPassword == ""){
      results.textContent = 'Password form is empty!';
      results.style.backgroundColor = 'red';
      passwordField.style.borderColor = 'red';
    } else if (inputLogin == "admin" && inputPassword == "12345"){
      results.textContent = 'Your are authorized!!!!';
      results.style.backgroundColor = 'green';
    }
  }

  authorization.addEventListener('click', checkInput);

} , false)